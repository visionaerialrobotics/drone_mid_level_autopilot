/*
 * YawController.h
 *
 *  Created on: 24/10/2012
 *      Author: Ignacio Mellado-Bataller
 */

#ifndef YAWCONTROLLER_H_
#define YAWCONTROLLER_H_

#include <math.h>
#include "control/PID.h"
#include <string>
#include <iostream>
#include "xmlfilereader.h"

namespace CVG {
namespace MAV {

class YawController {
public:
    YawController(int idDrone, const std::string &stackPath_in);
    void setFeedback(double yawMeasure_deg);
    double getOutput();
    void setReference(double yawRef_deg);
    void enable(bool e);
    inline bool isEnabled() { return enabled; }
    inline void setMaxAllowedOutput(double maxOutput) { yawPid.enableMaxOutput(true, maxOutput); }
    inline double getReference() { return reference; }
    double mapAngleToMinusPlusPi(double angleRads);

public:
    void init(std::string configFile);
    bool readConfigs(std::string configFile);

protected:
    void calcError();

private:
    CVG_BlockDiagram::PID yawPid;
    bool enabled;
    double reference, feedback;

    // Configuration parameters
    double GAIN_P, GAIN_I, GAIN_D;
};

}
}

#endif /* YAWCONTROLLER_H_ */
