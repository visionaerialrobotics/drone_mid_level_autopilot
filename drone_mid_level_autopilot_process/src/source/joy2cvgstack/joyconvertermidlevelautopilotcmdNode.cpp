//I/O stream
//std::cout
#include <iostream>

//ROS
#include "ros/ros.h"

//parrotARDrone
#include "joy2cvgstack/joyconvertermidlevelautopilotcmd.h"

using namespace std;
const std::string NODE_NAME_JOY_CONVERTER = "joyconvertermidlevelautopilotcmd";

int main(int argc,char **argv)
{
    //Ros Init
    ros::init(argc, argv, NODE_NAME_JOY_CONVERTER);
    ros::NodeHandle n;

    cout<<"[ROSNODE] Starting MyJoyConverterRODModule"<<endl;

    //Vars
    JoyConverterDrvPelicanCmd MyJoyConverterRODModule;
    MyJoyConverterRODModule.open(n,NODE_NAME_JOY_CONVERTER);

    try
    {
        //Read messages
        ros::spin();
        return 1;

    }
    catch (std::exception &ex)
    {
        std::cout<<"[ROSNODE] Exception :"<<ex.what()<<std::endl;
    }
}

