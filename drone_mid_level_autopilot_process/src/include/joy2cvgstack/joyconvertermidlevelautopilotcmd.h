#ifndef JOYCONVERTERDRVPELICANCMD_H
#define JOYCONVERTERDRVPELICANCMD_H

// This node outputs asctec_msgs::CtrlInput messages. This serves to test the AscTec drivers directly
//   (that is the Asctec Autopilot ROS Node).

#include "droneModuleROS.h"

//Input
#include "sensor_msgs/Joy.h"

//Output
#include "droneMsgsROS/dronePitchRollCmd.h"
#include "droneMsgsROS/droneDYawCmd.h"
#include "droneMsgsROS/droneDAltitudeCmd.h"
#include "communication_definition.h"

class JoyConverterDrvPelicanCmd : public DroneModule
{

    //Publisher
protected:
    ros::Publisher OutputPubl_PR;
    ros::Publisher OutputPubl_dY;
    ros::Publisher OutputPubl_dA;

    //Subscriber
protected:
    ros::Subscriber InputSubs;
    void inputCallback(const sensor_msgs::Joy::ConstPtr& msg);


    //Constructors and destructors
public:
    JoyConverterDrvPelicanCmd();
    ~JoyConverterDrvPelicanCmd();


    //Init and close
public:
    void init();
    void close();

    //Open
 public:
    void open(ros::NodeHandle & nIn, std::string moduleName);

    //Reset
protected:
    bool resetValues();

    //Start
protected:
    bool startVal();

    //Stop
protected:
    bool stopVal();

    //Run
public:
    bool run();


};



#endif // JOYCONVERTERDRVPELICANCMD_H
